package util;
import java.io.*;
import java.util.ArrayList;

import static java.lang.String.format;

public class TestUtils {

    public void createFile(String fileName, String[] dataLines)
    {
        try {
            FileWriter fileWriter = new FileWriter(fileName);
            BufferedWriter buffWriter = new BufferedWriter(fileWriter);

            for(String dataLine : dataLines)
            {
                dataLine = format("%s\n", dataLine);
                buffWriter.write(dataLine);
            }
            buffWriter.flush();
            buffWriter.close();
        }
        catch (Exception ex)
        {
            System.out.print("Errored");
        }
    }

    public void getFileContent(String fileName, ArrayList<String> dataLines)
    {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String line;
            while((line = reader.readLine()) != null)
            {
                dataLines.add(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void deleteFile(String fileName)
    {
        File file = new File(fileName);
        boolean delete = file.delete();

        if(delete == true)
        {
            System.out.println("Deleted");
        }
    }
}
