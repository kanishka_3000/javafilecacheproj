package app.io;

import app.io.impl.FileReader;


import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import util.TestUtils;
import static org.junit.Assert.assertThat;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.BaseMatcher.*;
public class TestFileReader {

    IoReader m_reader;
    TestUtils m_utils = new TestUtils();

    app.io.IOOutput m_output;

    @BeforeClass
    public static void beforeClass()
    {

    }
    @Before
    public void beforeTest()
    {
        m_reader = new app.io.impl.FileReader();
        m_output = PowerMockito.mock(app.io.IOOutput.class);
    }

    @After
    public void afterTest()
    {

    }

    @Test
    public void testFileReaderBasicFile()throws Exception
    {
            String[] lines = {"ABC", "EFT", "PQR"};
            m_utils.createFile("InputTestFile.txt", lines);
            ArrayList<String> outputs = new ArrayList<>();
            Answer<Object> obj = (InvocationOnMock var1) -> {
                Object[] data = var1.getArguments();
                System.out.print(data[0]);
                outputs.add((String)data[0]);
                return  null;
            };

            PowerMockito.doAnswer(obj).doAnswer(obj).when(m_output).onDataLine(any());

            //PowerMockito.doNothing().when(m_output).onDataLine("EFT");

            m_reader.init("InputTestFile.txt");
            while(m_reader.readNext(m_output) != IOStatus.ERR_ERROR)
            {

            }
            assertEquals(3 , outputs.size());
            assertThat(outputs, hasItems("ABC", "EFT", "PQR"));

            m_utils.deleteFile("InputTestFile.txt");
    }

    @Test
    public void whenFileReaderReadRandomIndex_thenCorrectValuesAreReturnred() throws IOException {

        String[] lines = {"45.44", "55.77", "545.23"};
        m_utils.createFile("InputTestFile2.txt", lines);

        m_reader.init("InputTestFile2.txt");
        double val = m_reader.getValue(2);

        org.junit.Assert.assertEquals(545.23, val, 0.001);

        m_utils.deleteFile("InputTestFile2.txt");

    }
}
