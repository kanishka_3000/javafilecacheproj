package app.io;

import app.io.impl.DataStoreInputFormat;
import org.junit.Test;

public class TestMiscPopulators {

    @Test
    public void testDataStoreInputFormat(){
        DataStoreInputFormat inputFormatter = new DataStoreInputFormat();
        inputFormatter.onDataLine("34 90");

        org.junit.Assert.assertEquals(34, inputFormatter.getIndex());
        org.junit.Assert.assertEquals(90, inputFormatter.getData(), 0.0001);
    }
}
