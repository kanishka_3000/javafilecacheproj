package app.io;

import app.io.impl.ReaderDumpInput;
import org.junit.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;

import util.TestUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestFileWriter {

    app.io.IOInput m_input;
    app.io.impl.FileWriter m_writer;

    TestUtils m_utils = new TestUtils();
    @BeforeClass
    public static void beforeClass()
    {

    }
    @AfterClass
    public static void afterClass()
    {

    }
    @Before
    public void before()
    {
        m_writer = new app.io.impl.FileWriter();
        m_input = PowerMockito.mock(app.io.IOInput.class);

    }
    @After
    public void after()
    {

    }

    @Test
    public void testFileWriterAppend()
    {
        m_writer.init("TestAppWriter.txt");
        PowerMockito.when(m_input.getData()).
                thenReturn("LMN").
                thenReturn("TPQ").
                thenReturn("RRA");

        m_writer.append(m_input);

        ArrayList<String> dataList = new ArrayList<String>();
        m_utils.getFileContent("TestAppWriter.txt", dataList);

        assertThat(dataList, hasItems("LMN"));

        dataList.clear();

        m_writer.append(m_input);
        m_utils.getFileContent("TestAppWriter.txt", dataList);

        assertThat(dataList, hasItems("LMN", "TPQ"));

        dataList.clear();

        m_writer.append(m_input);
        m_utils.getFileContent("TestAppWriter.txt", dataList);

        assertThat(dataList, hasItems("LMN", "TPQ", "RRA"));

        dataList.clear();

        m_utils.deleteFile("TestAppWriter.txt");
    }

    @Test
    public void whenDataInsertedToExitingIndex_thenTargetLineIsReplaced(){
        String[] dataLines = {"33","44", "98" , "56"};

        m_utils.createFile("ReplaFile.txt", dataLines);

        m_writer.init("ReplaFile.txt");
        m_writer.insert(2, new ReaderDumpInput(101.45));

        ArrayList<String> dataList = new ArrayList<String>();

        m_utils.getFileContent("ReplaFile.txt", dataList);

        org.junit.Assert.assertEquals(4, dataList.size());
        assertThat(dataList, hasItems("33", "44" , "101.45", "56"));

        for(String s : dataList)
        {
            System.out.println(s);
        }

        m_utils.deleteFile("ReplaFile.txt");
    }

    @Test
    public void whenDataLinesForNonExistantIndexInserted_thenCorrectLineIsReplaced() {
        String[] dataLines = {"33","44", "98" , "56"};

        m_utils.createFile("ReplaFile2.txt", dataLines);

        m_writer.init("ReplaFile2.txt");

        m_writer.insert(6, new ReaderDumpInput(101.45));

        ArrayList<String> dataList = new ArrayList<String>();

        m_utils.getFileContent("ReplaFile2.txt", dataList);

        ArrayList<String> expectedList = new ArrayList<>(Arrays.asList("33","44", "98" , "56" ,"0" , "0" , "101.45"));

        org.junit.Assert.assertEquals(expectedList, dataList);

        for(String s : dataList)
        {
            System.out.println(s);
        }

        m_utils.deleteFile("ReplaFile2.txt");
    }
}
