package app.io;

import app.core.DataManagerOutput;
import app.core.ReadSource;
import app.reader.impl.ReaderImpl;

import org.junit.*;
import org.mockito.Mockito;
import org.mockito.internal.matchers.Matches;
import org.mockito.invocation.InvocationOnMock;
import org.powermock.api.mockito.PowerMockito;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;

import java.io.FileNotFoundException;

public class TestReaderImpl {

    app.io.IoReader readSource;
    app.io.IOWriter output;
    app.core.DataManager dataManager;

    app.io.IOOutput dataOutput = null;

    app.reader.impl.ReaderImpl reader = null;
    @BeforeClass
    public static void setUpTestCase() {

    }

    @Before
    public void setUp() {

        readSource = PowerMockito.mock(app.io.IoReader.class);
        output = PowerMockito.mock(app.io.IOWriter.class);
        dataManager = PowerMockito.mock(app.core.DataManager.class);
        dataOutput = PowerMockito.mock(app.io.IOOutput.class);
        reader = new app.reader.impl.ReaderImpl(readSource, output, dataManager);
    }

    @After
    public void tearDown() {
        reader = null;
    }

    @AfterClass
    public static void tearDownTestCase() {

    }

    @Test
    public void whenInitializedWithError_thenCorrectCallsAreReceived() throws FileNotFoundException{

            PowerMockito.when(readSource.readNext(any())).
                    thenReturn(IOStatus.ERR_ERROR);

            reader.init("Reader1.txt", "Writer1.txt");

            Mockito.verify(readSource).init("Reader1.txt");
            Mockito.verify(output).init("Writer1.txt");

    }

    @Test
    public void whenInitializedWithoutError_thenCorrectCallsAreReceived() throws FileNotFoundException{

        PowerMockito.doAnswer(
                (InvocationOnMock var1) -> {
                    Object[] data = var1.getArguments();
                    System.out.println("Answered");
                    app.io.impl.InputData dt = (app.io.impl.InputData)data[0];
                    dt.onDataLine("25");
            return  IOStatus.ERR_NONE;
        }).
                doReturn(IOStatus.ERR_ERROR).
                when(readSource).readNext(any());

        PowerMockito.when(dataManager.getValue(25)).
                thenReturn(new DataManagerOutput(47.46, 1, ReadSource.CACHE));

        reader.init("Reader1.txt", "Writer1.txt");

        Mockito.verify(readSource).init("Reader1.txt");
        Mockito.verify(output).init("Writer1.txt");

        Mockito.verify(output).insert(eq(25), any());

    }


}
