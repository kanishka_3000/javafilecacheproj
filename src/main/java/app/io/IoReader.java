package app.io;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface IoReader {

    void init(String fileName) throws FileNotFoundException;
    void reInit(String fileName) throws FileNotFoundException;
    IOStatus readNext(IOOutput output);
    double getValue(int index) throws IOException;

}
