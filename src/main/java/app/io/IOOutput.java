package app.io;

public interface IOOutput {
    void onDataLine(String dataLine);
}
