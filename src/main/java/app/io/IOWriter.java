package app.io;

public interface IOWriter {

    void init(String fileName);
    void insert(int index, final IOInput input);
    void append(final IOInput input);

}
