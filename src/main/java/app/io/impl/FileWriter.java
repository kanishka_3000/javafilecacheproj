package app.io.impl;

import app.io.IOInput;
import app.io.IOWriter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileWriter implements IOWriter {

    String m_fileName;

    @Override
    public void init(String fileName) {
        m_fileName = fileName;
    }

    @Override
    public void insert(int index, IOInput input) {
        String tmpFileName = String.format("%s.tmp", m_fileName);
        java.io.File tmpFile = new java.io.File(tmpFileName);

        try {
            BufferedReader reader = new BufferedReader(new java.io.FileReader(m_fileName));
            BufferedWriter writer = new BufferedWriter(new java.io.FileWriter(tmpFile));

            int finalIndex = replaceIfExist(index, input, reader, writer);
            if(finalIndex < index)
            {
                padUptoAndReplace(finalIndex, index, input, writer);
            }
            writer.flush();
            //writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        tmpFile.renameTo(new java.io.File(m_fileName));
    }


    @Override
    public void append(IOInput input) {
        try {
            java.io.FileWriter fileWriter = new java.io.FileWriter(m_fileName, true);
            String dataLine = input.getData();
            dataLine = String.format("%s\n", dataLine);
            fileWriter.append(dataLine);
            fileWriter.flush();
            fileWriter.close();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }

    private void padUptoAndReplace(int currentIndex, int targetIndex, IOInput input, BufferedWriter writer)throws IOException{

        for(int idx = currentIndex; idx < targetIndex; idx++)
        {
            writer.write("0\n");
        }
        writer.write(input.getData() + "\n");
    }

    private int replaceIfExist(int index, IOInput input, BufferedReader reader, BufferedWriter writer) throws IOException {

        String dataLine;
        int currentIndex = 0;
        while((dataLine =reader.readLine()) != null)
        {
            if(currentIndex != index)
            {
                writer.write(dataLine + "\n");
            }
            else
            {
                writer.write(input.getData() + "\n");
            }
            currentIndex++;
        }

        return currentIndex;
    }

}
