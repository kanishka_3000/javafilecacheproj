package app.io.impl;

public class InputData implements app.io.IOOutput {

    String dataLine;

    @Override
    public void onDataLine(String dataLine) {
       this.dataLine = dataLine;
    }

    public double getValue()
    {
        double val = Double.valueOf(dataLine);
        return  val;
    }
}
