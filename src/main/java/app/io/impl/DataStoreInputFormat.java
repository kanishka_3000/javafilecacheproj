package app.io.impl;

import app.io.IOOutput;

public class DataStoreInputFormat implements IOOutput {

    int index;
    double data;

    @Override
    public void onDataLine(String dataLine) {

        String[] dataItems = dataLine.split(" ");
        index = Integer.valueOf(dataItems[0]);
        data = Double.valueOf(dataItems[1]);

    }

    public int getIndex(){
        return  index;
    }

    public double getData(){
        return  data;
    }
}
