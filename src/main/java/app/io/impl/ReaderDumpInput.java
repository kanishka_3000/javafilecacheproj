package app.io.impl;

import app.io.IOInput;

public class ReaderDumpInput implements IOInput {

    double value;

    public ReaderDumpInput(double value)
    {
        this.value = value;
    }
    @Override
    public String getData() {

        return Double.toString(this.value);
    }
}
