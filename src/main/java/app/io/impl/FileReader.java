package app.io.impl;

import java.io.*;

import app.io.IOOutput;
import app.io.IOStatus;

public class FileReader implements app.io.IoReader {

    BufferedReader m_reader = null;
    String fileName;

    @Override
    public void init(String fileName) throws FileNotFoundException{
        this.fileName = fileName;
        m_reader = new BufferedReader(new java.io.FileReader(fileName));
    }

    @Override
    public void reInit(String fileName) throws FileNotFoundException {
        init(fileName);
    }

    @Override
    public IOStatus readNext(IOOutput output) {

        String dataLine;
        try {
            if ((dataLine = m_reader.readLine()) != null) {

                    output.onDataLine(dataLine);
                    return IOStatus.ERR_NONE;
            }
            else
            {
                return IOStatus.ERR_ERROR;
            }
        }
        catch (IOException ex)
        {
            return IOStatus.ERR_ERROR;
        }

    }

    @Override
    public double getValue(int index) throws IOException {

        BufferedReader read = new BufferedReader(new java.io.FileReader(fileName));

        int currentIndex = 0;
        String dataLine;
        while ( (dataLine = read.readLine() ) != null)
        {
            if(currentIndex == index)
            {
                double val = Double.valueOf(dataLine);
                return val;
            }
            currentIndex++;
        }

       return 0;

    }
}
