package app.writer.impl;

import app.core.DataManager;
import app.io.IOStatus;
import app.io.impl.DataStoreInputFormat;

import java.io.FileNotFoundException;

public class WriterImpl implements app.writer.Writer {

    private DataManager dataManager = null;
    private app.io.IoReader fileReader = null;

    public WriterImpl(app.core.DataManager dataManager, app.io.IoReader fileReader) {
        this.dataManager = dataManager;
        this.fileReader = fileReader;
    }

    @Override
    public void init(String inputFileName) {

        try {

            fileReader.init(inputFileName);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        DataStoreInputFormat storeInput = new DataStoreInputFormat();
        while(fileReader.readNext(storeInput) != IOStatus.ERR_ERROR)
        {
            int index = storeInput.getIndex();
            double data = storeInput.getData();



        }
    }

}
