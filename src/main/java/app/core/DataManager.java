package app.core;

public interface DataManager {

    DataManagerOutput getValue(final int index) ;
    void onNewValue(int index, double value);
}
