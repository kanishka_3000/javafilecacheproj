package app.core;

public class CacheOutput{

    private CacheResult result;
    private double value;

    public CacheOutput(CacheResult result, double value) {
        this.setResult(result);
        this.setValue(value);
    }

    public CacheResult getResult() {
        return result;
    }

    public CacheOutput setResult(CacheResult result) {
        this.result = result;
        return this;
    }

    public double getValue() {
        return value;
    }

    public CacheOutput setValue(double value) {
        this.value = value;
        return this;
    }
}
