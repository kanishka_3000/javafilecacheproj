package app.core;

public class DataManagerOutput {

    private double value;
    private int status;
    private ReadSource source;

    public DataManagerOutput(double value, int status, ReadSource source) {
        this.value = value;
        this.status = status;
        this.source = source;
    }

    public double getValue() {
        return value;
    }

    public int getStatus() {
        return status;
    }

    public ReadSource getSource() {
        return source;
    }
}
