package app.core;

public interface Cache {

    void init(int size);
    CacheOutput getValue(int index);
    void onNewalue(int index, double value);
}
