package app.core.impl;

import app.core.*;
import app.io.IOWriter;
import app.io.IoReader;
import app.io.impl.ReaderDumpInput;

import java.io.IOException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class DataManagerImpl implements DataManager {

    Cache cache = null;
    IoReader reader = null;
    IOWriter writer = null;

    ReadWriteLock lock = new ReentrantReadWriteLock();
    Lock readLock = null;
    Lock writeLock = null;

    public DataManagerImpl(Cache cache, IoReader reader, IOWriter writer) {

        readLock = lock.readLock();
        writeLock = lock.writeLock();

    }

    @Override
    public DataManagerOutput getValue(int index) {

        readLock.lock();
        CacheOutput output = cache.getValue(index);
        readLock.unlock();

        if(output.getResult() == CacheResult.HIT)
        {
            DataManagerOutput dmOutput = new DataManagerOutput(output.getValue(), 1, ReadSource.CACHE);
            return dmOutput;
        }

        try{

            double value = reader.getValue(index);
            DataManagerOutput dmOutput = new DataManagerOutput(value, 1, ReadSource.FILE);

            writeLock.lock();
            cache.onNewalue(index, value);
            writeLock.unlock();

        }
        catch (java.io.IOException ex)
        {
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public void onNewValue(int index, double value) {

        writeLock.lock();

        cache.onNewalue(index, value);
        writer.insert(index, new ReaderDumpInput(value));

        writeLock.unlock();
    }
}
