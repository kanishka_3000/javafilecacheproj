package app.reader.impl;

import app.core.DataManager;
import app.core.DataManagerOutput;
import app.core.ReadSource;
import app.io.IOStatus;
import app.reader.Reader;
import app.io.IoReader;
import app.io.IOWriter;

import java.io.FileNotFoundException;

public class ReaderImpl implements Reader {

    app.io.IoReader readSource;
    app.io.IOWriter output;
    app.core.DataManager dataManager;

    public ReaderImpl(app.io.IoReader readSource, app.io.IOWriter output, DataManager dataManager){
        this.readSource = readSource;
        this.output = output;
        this.dataManager = dataManager;
    }

    @Override
    public void init(String inputFileName, String outputFileName) {

        try {
            readSource.init(inputFileName);
            output.init(outputFileName);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        app.io.impl.InputData data = new app.io.impl.InputData();
        while (readSource.readNext(data) != IOStatus.ERR_ERROR){

                int index = (int)data.getValue();
                DataManagerOutput dmOutput = dataManager.getValue(index);

                int dataStatus = dmOutput.getStatus();

                if(dataStatus  != -1) {
                    output.insert(index, new app.io.impl.ReaderDumpInput(dmOutput.getValue()));
                }
        }

    }
}
