package app.reader;

public interface Reader {

     void init(String inputFileName, String outputFileName) ;
}
